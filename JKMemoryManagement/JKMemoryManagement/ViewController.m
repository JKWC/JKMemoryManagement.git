//
//  ViewController.m
//  JKMemoryManagement
//
//  Created by 王冲 on 2018/10/26.
//  Copyright © 2018年 JK科技有限公司. All rights reserved.
//

#import "ViewController.h"
#import "GCDTimerViewController.h"
#import "TestViewController.h"
// JKGCDTimer 定时器封装的使用控制器
#import "JKGCDTimerUseViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"GCDTimer" style:UIBarButtonItemStylePlain target:self action:@selector(clickGCDTimer)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"JKGCDTimer" style:UIBarButtonItemStylePlain target:self action:@selector(clickJKGCDTimer)];
}

#pragma mark CADisplayLink 与 NSTimer 对比的使用，以及彼此循环引用的处理办法（NSProxy:中间对象）
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    TestViewController *vc = [TestViewController new];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark clickGCDTimer 没有封装前定时器的使用
-(void)clickGCDTimer{
    
    GCDTimerViewController *gcdTimerViewController = [GCDTimerViewController new];
    [self.navigationController pushViewController:gcdTimerViewController animated:YES];
    
}

#pragma mark JKGCDTimer 封装好的定时器的使用
-(void)clickJKGCDTimer{
    
    JKGCDTimerUseViewController *jkGCDTimerUseViewController = [JKGCDTimerUseViewController new];
    [self.navigationController pushViewController:jkGCDTimerUseViewController animated:YES];
}



@end
