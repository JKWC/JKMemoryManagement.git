//
//  TestViewController.m
//  JKMemoryManagement
//
//  Created by 王冲 on 2018/10/26.
//  Copyright © 2018年 JK科技有限公司. All rights reserved.
//

#import "TestViewController.h"
#import "JKMiddleProxy.h"
#import "JKProxy.h"
@interface TestViewController ()

@property(nonatomic,strong) CADisplayLink *link;

@property(nonatomic,strong) NSTimer *timer;

@end

@implementation TestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];

    // 保证调用频率和屏幕的刷帧频率一致 60FPS
    /*
    self.link = [CADisplayLink displayLinkWithTarget:self selector:@selector(linkTest)];
    [self.link addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
    */
    
    // self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTest) userInfo:nil repeats:YES];
    
    /*  解决办法 1
    __weak typeof(self) weakSelf = self;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
        
        [weakSelf timerTest];
    }];
     */
    
    // 解决办法 2 代理对象
    //self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:[JKMiddleProxy proxyWithTarget:self] selector:@selector(timerTest) userInfo:nil repeats:YES];
    
    // 解决办法 3 Proxy:更加高效的代理对象
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:[JKProxy proxyWithTarget:self] selector:@selector(timerTest) userInfo:nil repeats:YES];
    
    
}

-(void)linkTest{
    
    NSLog(@"%s",__func__);
}

-(void)timerTest{
    
    NSLog(@"%s",__func__);
}

-(void)dealloc{
    
    NSLog(@"%s", __func__);
    
    // [self.link invalidate];
    
    [self.timer invalidate];
    
    
}

@end
