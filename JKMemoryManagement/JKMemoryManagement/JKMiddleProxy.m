//
//  JKMiddleProxy.m
//  JKMemoryManagement
//
//  Created by 王冲 on 2018/10/26.
//  Copyright © 2018年 JK科技有限公司. All rights reserved.
//

#import "JKMiddleProxy.h"

@implementation JKMiddleProxy

+ (instancetype)proxyWithTarget:(id)target
{
    JKMiddleProxy *proxy = [[JKMiddleProxy alloc] init];
    proxy.target = target;
    return proxy;
}

- (id)forwardingTargetForSelector:(SEL)aSelector
{
    return self.target;
}

@end
