//
//  JKProxy.m
//  JKMemoryManagement
//
//  Created by 王冲 on 2018/10/27.
//  Copyright © 2018年 JK科技有限公司. All rights reserved.
//

#import "JKProxy.h"

@implementation JKProxy

+ (instancetype)proxyWithTarget:(id)target
{
    // NSProxy对象不需要调用init，因为它本来就没有init方法
    JKProxy *proxy = [JKProxy alloc];
    proxy.target = target;
    return proxy;
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)sel
{
    return [self.target methodSignatureForSelector:sel];
}

- (void)forwardInvocation:(NSInvocation *)invocation
{
    [invocation invokeWithTarget:self.target];
}
@end
