//
//  GCDTimerViewController.m
//  JKMemoryManagement
//
//  Created by 王冲 on 2018/10/27.
//  Copyright © 2018年 JK科技有限公司. All rights reserved.
//

#import "GCDTimerViewController.h"

@interface GCDTimerViewController ()

@property(nonatomic,strong) dispatch_source_t gcdTimer;
@end

@implementation GCDTimerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
     self.view.backgroundColor = [UIColor whiteColor];
    
     // 创建队列
     dispatch_queue_t queue = dispatch_get_main_queue();
    
     // 创建定时器
     self.gcdTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    
     // 设置时间
     /*
      dispatch_source_t  _Nonnull source: 定时器
      dispatch_time_t start: 开始的时间,dispatch_time(DISPATCH_TIME_NOW, start * NSEC_PER_SEC)，start多长时间后开始，NSEC_PER_SEC(纳秒)
      uint64_t interval：时间间隔
      uint64_t leeway: 误差，写0就好
      */
    uint64_t start = 2.0;
    uint64_t interval = 1.0;
    dispatch_source_set_timer(self.gcdTimer, dispatch_time(DISPATCH_TIME_NOW, start * NSEC_PER_SEC),  interval * NSEC_PER_SEC,0);
    
    
     // 设置回调1
    static int count = 0;
    dispatch_source_set_event_handler(self.gcdTimer, ^{
        
        count ++;
        NSLog(@"count== %d",count);
        
    });
    
    // 设置回调2
    //dispatch_source_set_event_handler_f(self.gcdTimer, timerFire);
    
    // 启动定时器
    dispatch_resume(self.gcdTimer);
}

// 设置回调2的定时器打印
void timerFire(void *param)
{
    NSLog(@"定时器打印 - %@", [NSThread currentThread]);
}


@end
